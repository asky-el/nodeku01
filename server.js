const express = require('express');

const app = express();

app.set('view engine', 'ejs');

app.get("/", function (request, resolve){
	var title = "Halaman Utama";
	resolve.render("index", {
	   title: title
	})
});

app.get("/profile", function (request, resolve){
	var title = "Halaman Profil";
	resolve.render("profile", {
	   title: title
	})
});

app.use(express.static('public'));

app.listen(3000, function() {
	console.log('aplikasi ini berjalan di port 3000');
});

